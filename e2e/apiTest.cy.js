describe('API Authentication & Authorization Test', () => {
  it('Should filter, count, and verify objects with "Authentication & Authorization"', () => {
    // Make a request to the API
    cy.request('GET', 'https://api.publicapis.org/entries')
      .then((response) => {
        // Extract the JSON body from the response
        const responseJson = response.body;

        // Filter objects with "Category" equal to "Authentication & Authorization"
        const auth2 = responseJson.entries.filter((entry) => {
          return entry.Category === 'Authentication & Authorization';
        });

        // Count the number of filtered objects
        const countAuth = auth2.length;

        // Log the filtered objects and the count
        cy.log('Filtered Objects:', auth2);
        cy.log('Number of Objects with "Authentication & Authorization":', countAuth);

        // Assert that at least one object is found
        expect(countAuth).to.be.above(0);
      });
  });
});
