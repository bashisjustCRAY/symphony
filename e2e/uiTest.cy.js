describe('Alphabetical Sorting Test', () => {
    beforeEach(() => {
      // Visit the Sauce Demo website
      cy.visit('https://www.saucedemo.com');
      cy.viewport('macbook-15');
      //cy.window().invoke('fullscreen');
    });
  
    it('Should log in and verify sorting by Name (A -> Z)', () => {
      // Log in with valid credentials
      cy.get('#user-name').type('standard_user');
      cy.get('#password').type('secret_sauce');
      cy.get('#login-button').click();
  
      // Verify items are sorted by Name (A -> Z)
      cy.get('.inventory_item_name').then(($elements) => {
      const initialOrder = $elements.toArray().map((el) => el.innerText);
      const sortedOrder = [...initialOrder].sort();
  
      expect(initialOrder).to.deep.equal(sortedOrder);
  });
  
    });
  
    it('Should log in and verify sorting by Name (Z -> A)', () => {
      // Log in with valid credentials
      cy.get('#user-name').type('standard_user');
      cy.get('#password').type('secret_sauce');
      cy.get('#login-button').click();
      cy.get('.product_sort_container').select('za');
  
      // Verify items are sorted by Name (Z -> A)
      cy.get('.inventory_item_name').then(($elements) => {
          const initialOrder = $elements.toArray().map((el) => el.innerText)
          const reverseOrder = [...initialOrder].sort().reverse();
          expect(initialOrder).to.deep.equal(reverseOrder);
        });
    });
  });
  