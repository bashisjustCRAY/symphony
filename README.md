# SauceDemo Sorting Test

Automated tests for SauceDemo sorting.

## Prerequisites
- Node.js and npm installed

## Installation
1. Clone the repository
2. Run `npm install`

## Run Tests Locally
- Run `npm test` to execute the tests locally

## Run Tests on CI
- Execute `npm run test-ci` on your CI platform

## View Test Reports
1. After running tests, execute `npm run merge-reports` to merge test reports
2. Execute `npm run generate-report` to generate an HTML report
3. Open the generated HTML report located at `cypress/results/mochawesome.html`